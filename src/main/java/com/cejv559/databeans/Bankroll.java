package com.cejv559.databeans;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

/**
 * This class represents the current amount of money that you have and the
 * current bet. When the amount of money drops below zero the game will end.
 *
 * This class also contains the state of the Pass Line game, playing for point
 * or not, and the value of the point.
 *
 * Session scoped so that the same Bankroll is used or every game
 *
 * @author Ken
 */
@Named
@SessionScoped
public class Bankroll implements Serializable {

    private BigDecimal money;
    private BigDecimal bet;
    private boolean playPoint;
    private int point;
    private String outcome;

    /**
     * Constructor that initializes money to a BigDecimal value of zero.
     */
    public Bankroll() {
        money = BigDecimal.ZERO;
        playPoint = false;
        outcome = "";
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    public BigDecimal getBet() {
        return bet;
    }

    public void setBet(BigDecimal bet) {
        this.bet = bet;
    }

    public boolean isPlayPoint() {
        return playPoint;
    }

    public void setPlayPoint(boolean playPoint) {
        this.playPoint = playPoint;
    }

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    public String getOutcome() {
        return outcome;
    }

    public void setOutcome(String outcome) {
        this.outcome = outcome;
    }
    
    

    @Override
    public String toString() {
        return "Bankroll{" + "money=" + money + ", bet=" + bet + ", playPoint=" + playPoint + ", point=" + point + '}';
    }
}
