/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cejv559.databeans;

import java.io.Serializable;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

/**
 * This is a hybrid class. It contains the values for both die and it contains a
 * roll method that generates the roll of the dice. A user must roll the dice
 * and then retrieve the values.
 *
 * Session scoped so that the same Dice are used for every game played.
 *
 * @author Ken Fogel
 */
@Named
@SessionScoped
public class Dice implements Serializable {

    private int die1;
    private int die2;
    private String classNameDieOne;
    private String classNameDieTwo;
    /**
     * Constructor that rolls the dice so that they will not be zero.
     */
    public Dice() {
        rollTheDice();
    }

    public int getDie1() {
        return die1;
    }

    public void setDie1(int die1) {
        this.die1 = die1;
    }

    public int getDie2() {
        return die2;
    }

    public void setDie2(int die2) {
        this.die2 = die2;
    }

    public String getClassNameDieOne() {
        this.classNameDieOne = generateString(this.die1);
        return classNameDieOne;
    }

    public void setClassNameDieOne(String classNameDieOne) {
        this.classNameDieOne = classNameDieOne;
    }

    public String getClassNameDieTwo() {
        this.classNameDieTwo = generateString(this.die2);
        return classNameDieTwo;
    }

    public void setClassNameDieTwo(String classNameDieTwo) {
        this.classNameDieTwo = classNameDieTwo;
    }
    
    

    /**
     * When only the total is important
     *
     * @return
     */
    public int getDiceTotal() {
        return die1 + die2;
    }

    /**
     * Roll the dice
     */
    public void rollTheDice() {
        die1 = (int) (Math.random() * 6.0) + 1;
        die2 = (int) (Math.random() * 6.0) + 1;
    }
    
    public String generateString(int die){
    String classString ="";
    switch(die){
        case 1:
            classString = "fi-die-one";
            break;
        case 2:
            classString = "fi-die-two";
            break;
        case 3:
            classString = "fi-die-three";
            break;
        case 4:
            classString = "fi-die-four";
            break;
        case 5:
            classString = "fi-die-five";
            break;
        case 6:  
            classString = "fi-die-six";
            break;
    }
    
    return classString;
    }

    @Override
    public String toString() {
        return "Dice{" + "die1=" + die1 + ", die2=" + die2 + '}';
    }

}
