package com.cejv559.games;

import com.cejv559.databeans.Bankroll;
import com.cejv559.databeans.Dice;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Locale;
import java.util.ResourceBundle;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

/**
 * The logic for the three games are kept here
 *
 * @author Ken
 */
@Named
@RequestScoped
public class TheGames implements Serializable {

    @Inject
    private Dice dice;

    @Inject
    private Bankroll bankroll;

    /**
     * The Field Bet game
     *
     * @return
     */
    public String fieldBet() {
        String nextPage = "";
        
        dice.rollTheDice();

        switch (dice.getDiceTotal()) {
            case 3:
            case 4:
            case 9:
            case 10:
            case 11:
                bankroll.setMoney(bankroll.getMoney().add(bankroll.getBet()));
                nextPage = "fieldBetWin";
                break;
            case 2:
                bankroll.setMoney(bankroll.getMoney().add(bankroll.getBet().multiply(new BigDecimal("2"))));
                nextPage = "fieldDoubleWin";
                break;
            case 12:
                bankroll.setMoney(bankroll.getMoney().add(bankroll.getBet().multiply(new BigDecimal("3"))));
                nextPage = "fieldTripleWin";
                break;
            case 5:
            case 6:
            case 7:
            case 8:
                bankroll.setMoney(bankroll.getMoney().subtract(bankroll.getBet()));
                nextPage = "fieldLose";
                break;
        }

        // Zero the bet. Not required, you may leave the bet amount for subsequent games.
        bankroll.setBet(BigDecimal.ZERO);

        return nextPage; // return the page that displays the result of this game
    }

    /**
     * The Any 7 game
     *
     * @return
     */
    public String any7() {
        String nextPage;
        
        dice.rollTheDice();

        if (dice.getDiceTotal() == 7) {
            bankroll.setMoney(bankroll.getMoney().add(bankroll.getBet().multiply(new BigDecimal("4"))));
            nextPage = "any7Win";
        } else {
            bankroll.setMoney(bankroll.getMoney().subtract(bankroll.getBet()));
            nextPage = "any7Lose";
        }

        // Zero the bet. Not required, you may leave the bet amount for subsequent games.
        bankroll.setBet(BigDecimal.ZERO);

        return nextPage; // return the page that displays the result of this game
    }

    /**
     * The Pass Line game
     *
     * @return
     */
    public String passLine() {

        // Depending on the outcome of the roll, this is the name of the next page to display
        String nextPage;
        FacesContext context = FacesContext.getCurrentInstance();
        Locale currentLocale = context.getViewRoot().getLocale();
        ResourceBundle msg = ResourceBundle.getBundle("com.cejv559.bundles.messages", currentLocale);

        dice.rollTheDice();

        // First pass
        if (bankroll.isPlayPoint() == false) {
            switch (dice.getDiceTotal()) {
                case 7:
                case 11: // Win your bet
                    bankroll.setMoney(bankroll.getMoney().add(bankroll.getBet()));
                    bankroll.setOutcome(msg.getString("comeOutRollWin"));
                    nextPage = "passLineWin";
                    break;
                case 2:
                case 3:
                case 12: // Lose your bet
                    bankroll.setMoney(bankroll.getMoney().subtract(bankroll.getBet()));
                    bankroll.setOutcome(msg.getString("comeOutRollLose"));
                    nextPage = "passLineLose";
                    break;
                default: // Set the point and keep playing
                    bankroll.setPoint(dice.getDiceTotal());
                    bankroll.setPlayPoint(true);
                    bankroll.setOutcome(msg.getString("pointKeepPlaying"));
                    nextPage = "passLineAgain";
                    break;
            }
        } else { // Playing for the point
            if (dice.getDiceTotal() == 7) { // You lose
                bankroll.setMoney(bankroll.getMoney().subtract(bankroll.getBet()));
                bankroll.setPlayPoint(false);
                bankroll.setOutcome(msg.getString("loseFailingPoint"));
                nextPage = "passLineLose";
            } else if (dice.getDiceTotal() == bankroll.getPoint()) { // You win
                bankroll.setMoney(bankroll.getMoney().add(bankroll.getBet()));
                bankroll.setPlayPoint(false);
                bankroll.setOutcome(msg.getString("winMakingPoint"));
                nextPage = "passLineWin";
            } else { // Neither win or lose so roll for the point again
                bankroll.setOutcome(msg.getString("rollForPointAgain"));
                nextPage = "passLineAgain";
            }
        }
        return nextPage;
    }
}
